" auto-install vim-plug
if empty(fnamemodify(expand('<sfile>'), ':h').'/autoload/plug.vim')
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall
endif



call plug#begin('~/.config/nvim/plugged')

Plug 'iCyMind/NeoSolarized'
Plug 'tpope/vim-sensible'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'itchyny/vim-cursorword'
Plug 'tpope/vim-sleuth'
Plug 'qba10/vim-sidemenu'
Plug 'qba10/vim-session'
Plug 'xolox/vim-misc'
Plug 'tpope/vim-fugitive'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'mhinz/vim-signify'
Plug 'justinmk/vim-syntax-extra'
Plug 'thinca/vim-localrc'
Plug 'mileszs/ack.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'Shougo/neoinclude.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'vivien/vim-linux-coding-style'
Plug 'EESchneider/vim-rebase-mode'
" snipmate dependencies
Plug 'tomtom/tlib_vim'
Plug 'marcweber/vim-addon-mw-utils'
Plug 'garbas/vim-snipmate'
"Lazy load plugins:
"
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'Xuyuanp/nerdtree-git-plugin', { 'on': 'NERDTreeToggle'}

Plug 'davidhalter/jedi-vim', { 'for': 'python' }
	let g:jedi#completions_enabled = 0
	let g:jedi#auto_vim_configuration = 0
	let g:jedi#smart_auto_mappings = 0
	let g:jedi#show_call_signatures = 1
	let g:jedi#use_tag_stack = 0
	let g:jedi#popup_select_first = 0
	let g:jedi#popup_on_dot = 0
	let g:jedi#max_doc_height = 100
	let g:jedi#quickfix_window_height = 10
	let g:jedi#use_splits_not_buffers = 'right'
Plug 'raimon49/requirements.txt.vim', {'for': 'requirements'}
Plug 'vim-python/python-syntax', {'for': 'python'}
let g:python_highlight_all = 1
Plug 'Vimjas/vim-python-pep8-indent', {'for': 'python'}
Plug 'vim-scripts/python_match.vim',{'for': 'python'}
Plug 'tell-k/vim-autopep8', {'for': 'python', 'do': 'pip3 install -U autopep8'}
Plug 'ekalinin/Dockerfile.vim', {'for': [ 'Dockerfile', 'yaml.docker-compose' ]}
Plug 'plasticboy/vim-markdown', {'for' : 'markdown'}


call plug#end()

execute 'source' fnamemodify(expand('<sfile>'), ':h').'/plugins/ctrlspace.vim'
execute 'source' fnamemodify(expand('<sfile>'), ':h').'/plugins/vimindent.vim'
execute 'source' fnamemodify(expand('<sfile>'), ':h').'/plugins/vim-sidemenu.vim'
